import type { GetStaticPaths, GetStaticPropsContext, InferGetStaticPropsType } from 'next';
import { PokemonClient } from 'pokenode-ts';

const api = new PokemonClient();
const POKEMON_COUNT = 150;

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: new Array(POKEMON_COUNT).fill(0).map((_, idx) => ({
      params: { id: `${idx + 1}` },
    })),
    fallback: false,
  };
};

export const getStaticProps = async ({ params }: GetStaticPropsContext) => {
  const pokemon = await api.getPokemonById(Number(params?.id));
  return {
    props: {
      name: pokemon.name,
      moves: pokemon.moves.map((move) => move.move.name),
    },
  };
};

const PokemonPage = ({ name, moves }: InferGetStaticPropsType<typeof getStaticProps>) => {
  return (
    <>
      <h1>{name}</h1>
      {moves.join(' ')}
    </>
  );
};

export default PokemonPage;
